package ru.kuzin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.dto.model.TaskDTO;

import java.util.List;

@NoArgsConstructor
public final class TaskListResponse extends AbstractTaskResponse {

    @Getter
    @Setter
    @Nullable
    private List<TaskDTO> tasks;

    public TaskListResponse(@NotNull final List<TaskDTO> tasks) {
        this.tasks = tasks;
    }

}