package ru.kuzin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskDtoRepository extends IUserOwnedDtoRepository<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}